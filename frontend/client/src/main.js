import Vue from "vue";
import App from "./App.vue";
import "./plugins/vuetify";
import router from "@/router/router";
import store from "@/vuex/store.js";

Vue.config.productionTip = false;

import vueLogger from "vuejs-logger";

const options = {
  isEnabled: true,
  logLevel: "debug",
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: false,
  separator: "|",
  showConsoleColors: true
};
Vue.use(vueLogger, options);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
