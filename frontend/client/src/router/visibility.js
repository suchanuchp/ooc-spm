export const visibilityNav = {
  userShouldSeeWhenLoggedIn: [
    "Log out",
    "My Analysis",
    "Shared Analysis",
    "Scan"
  ],
  userShouldSeeWhenLoggedout: ["Sign up", "Log in"]
};
