import Vue from "vue";
import Router from "vue-router";
import store from "@/vuex/store";
import { visibilityNav } from "./visibility";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "landing page",
      component: () => import("../components/LandingPage/LandingPage"),
      meta: {
        title: "Spectrophotometer",
        public: true
      }
    },

    {
      path: "/register",
      name: "register",
      component: () => import("../components/Register/Register"),
      meta: {
        title: "- Register Page -",
        public: true,
        onlyWhenLoggedOut: true
      }
    },
    {
      path: "/login",
      name: "loginPage",
      component: () => import("../components/Login/Login"),
      meta: {
        title: "- Login Page -",
        public: true,
        onlyWhenLoggedOut: true
      }
    },
    {
      path: "/scan",
      name: "scanPage",
      component: () => import("../components/Scan/Scan"),
      meta: { title: "- Scan Page -", public: false }
    },
    {
      path: "/analysis",
      name: "analysis",
      component: () => import("../components/AnalysisPage/AnalysisPage"),
      meta: { title: "- Analysis -", public: false }
    },
    {
      path: "/sharedAnalysis",
      name: "sharedAnalysis",
      component: () => import("../components/AnalysisPage/SharedAnalysis"),
      meta: { title: "- Shared Analysis - ", public: false }
    },
    {
      path: "/analysis/:id/:type",
      name: "analysis_particular",
      component: () => import("../components/GraphInfo/GraphInfo"),
      meta: { title: "- Analysis Card - ", public: false },
      props: true
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/About"),
      meta: { title: "- About", public: false }
    },
    { path: "/logout", name: "logout" },

    {
      path: "*",
      // must be 404
      redirect: "/"
    }
  ]
});

const DEFAULT_TITLE = "Default Title";
router.afterEach((to, from) => {
  document.title = to.meta.title || DEFAULT_TITLE;
});

router.beforeEach((to, from, next) => {
  store.dispatch("fetchAccessToken");
  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(
    record => record.meta.onlyWhenLoggedOut
  );
  // const loggedIn = !!TokenService.getToken();
  const loggedIn = !!store.state.accessToken;
  if (!isPublic && !loggedIn) {
    return next({
      path: "/login",
      query: { redirect: to.fullPath } // Store the full path to redirect the user to after login
    });
  }

  if (loggedIn && to.fullPath === "/logout") {
    store.dispatch("logout");
    localStorage.clear();

    store.dispatch("toggleListOfMenu", {
      listOfMenus: visibilityNav.userShouldSeeWhenLoggedIn,
      bool_: false
    });
    store.dispatch("toggleListOfMenu", {
      listOfMenus: visibilityNav.userShouldSeeWhenLoggedout,
      bool_: true
    });

    return next("/");
  }

  // Do not allow user to visit login page or register page if they are logged in
  if (loggedIn && onlyWhenLoggedOut) {
    return next("/");
  }

  next();
});

export default router;
