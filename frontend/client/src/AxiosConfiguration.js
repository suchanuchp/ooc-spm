import axios from "axios";
import qs from "qs";

//const BASE_URL = "http://localhost:9000";
const BASE_URL = process.env.NODE_ENV === "production" ? "/api" : "http://localhost:9000/api";

const AXIOS_CONF = {
  baseURL: BASE_URL,
  withCredentials: true,
  transformRequest: [
    function(data) {
      return qs.stringify(data);
    }
  ]
};

export default new axios.create(AXIOS_CONF);

export const axiosUploader = axios.create({
  ...AXIOS_CONF,
  transformRequest: []
});

export const axiosDownloader = axios.create({
  ...AXIOS_CONF,
  transformRequest: [],
  responseType: "blob"
});
