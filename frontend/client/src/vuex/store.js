import Vue from "vue";
import Vuex from "vuex";
import axios from "@/AxiosConfiguration";
import createPersistedState from "vuex-persistedstate";
import router from "@/router/router";

// Make vue aware of Vuex
Vue.use(Vuex);

const state = {
  navStat: [
    { title: "Sign up", link: "/register", visible: true },
    { title: "Log in", link: "/login", visible: true },
    { title: "My Analysis", link: "/analysis", visible: false },
    {
      title: "Shared Analysis",
      link: "/sharedAnalysis",
      visible: false
    },
    { title: "Scan", link: "/scan", visible: false },
    { title: "About us", link: "/about", visible: false },
    { title: "Log out", link: "/logout", visible: false, icon: "exit_to_app" }
  ],
  accessToken: null,
  loggingIn: false,
  loginError: null
};

const mutations = {
  LOGIN_START(state) {
    state.loggingIn = true;
  },
  // Change to link to be visible or invisible, pass in an object of title and boolean for visibility.
  // payload = {nav : {} , bool_ : true/false}
  SET_VISIBLE(state, { nav, boolVal }) {
    nav.visible = boolVal;
  },
  LOG_OUT(state) {
    state.accessToken = null;
  },
  LOGIN_STOP(state, err) {
    state.loggingIn = false;
    state.loginError = err;
  },
  UPDATE_TOKEN(state, token) {
    state.accessToken = token;
  }
};

const getters = {
  filterMenus(state) {
    return state.navStat.filter(x => x.visible);
  }
};

const actions = {
  doLogin({ commit, dispatch }, loginData) {
    commit("LOGIN_START");
    axios
      .post("/login", loginData)
      .then(response => {
        localStorage.setItem("accessToken", response.config.xsrfCookieName);
        commit("LOGIN_STOP", null);
        commit("UPDATE_TOKEN", response.config.xsrfCookieName);
        const userWillSeeThese = [
          "My Analysis",
          "Shared Analysis",
          "Log out",
          "Scan"
        ];
        const userNotSeeThese = ["Sign up", "Log in"];

        dispatch("toggleListOfMenu", {
          listOfMenus: userWillSeeThese,
          bool_: true
        });

        dispatch("toggleListOfMenu", {
          listOfMenus: userNotSeeThese,
          bool_: false
        });

        router.push("/");
      })
      .catch(error => {
        commit("LOGIN_STOP", "Username or password is wrong");
        commit("UPDATE_TOKEN", null);
      });
  },
  logout({ commit }) {
    localStorage.removeItem("accessToken");
    // add get request to api
    axios.get("/logout").then(res => console.log(res));
    commit("LOG_OUT");
    router.push("/");
  },
  fetchAccessToken({ commit }) {
    commit("UPDATE_TOKEN", localStorage.getItem("accessToken"));
  },
  toggleListOfMenu({ commit, state }, { listOfMenus, bool_ }) {
    for (let i = 0; i < listOfMenus.length; i++) {
      let findIt = state.navStat.find(r => r.title === listOfMenus[i]);
      if (findIt) commit("SET_VISIBLE", { nav: findIt, boolVal: bool_ });
    }
  }
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  plugins: [createPersistedState()]
});
