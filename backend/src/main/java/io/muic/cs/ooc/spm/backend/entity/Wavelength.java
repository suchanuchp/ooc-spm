package io.muic.cs.ooc.spm.backend.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="wavelength")
public class Wavelength {

    @Id
    @GeneratedValue
    private Long wavelengthId;


    private Integer length;

    public Wavelength(){}

    public Wavelength(Integer length){
        this.length = length;
    }

    public Integer getLength() {
        return length;
    }
}
