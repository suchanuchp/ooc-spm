package io.muic.cs.ooc.spm.backend.entity;

public enum MethodType {
    lambda,
    scan
}
