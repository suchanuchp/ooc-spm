package io.muic.cs.ooc.spm.backend.controller;


import io.muic.cs.ooc.spm.backend.dto.SignalsDTO;
import io.muic.cs.ooc.spm.backend.entity.User;
import io.muic.cs.ooc.spm.backend.service.ImageService;
import io.muic.cs.ooc.spm.backend.service.ScanService;


import io.muic.cs.ooc.spm.backend.service.SignalService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * ScanController handles incoming requests to
 * record the (lambda/scan) measurement of a sample,
 * obtain wavelength-to-absorbance and -to-transmittance values,
 * and download a recorded measurement as a file.
 */
@RestController
public class ScanController {

    @Autowired
    private ScanService scanService;

    @Autowired
    private SignalService signalService;


    /**
     * Handles requests to record the measurement of a sample.
     *
     * @param method Method type: lambda, scan
     * @param blankImage
     * @param sampleImage
     * @param sampleName
     * @param wavelengths specified wavelengths, or scan (400-700 nm in increments of 1 nm)
     * @param emails A list of emails to share the measurement with
     * @param auth
     * @return An ID of the recorded measurement
     * @throws IOException
     */
    @PostMapping(value = "/scan")
    public Long measure(@RequestParam("method") String method,
                                  @RequestParam("blankImg") MultipartFile blankImage,
                                  @RequestParam("sampleImg") MultipartFile sampleImage,
                                  @RequestParam("sampleName") String sampleName,
                                  @RequestParam(value="wavelengths", required=false) ArrayList<Integer> wavelengths,
                                  Authentication auth) throws IOException {

        if(wavelengths == null){
            wavelengths = ImageService.DEFAULT_WAVELENGTHS;
        }

        User user = (User) auth.getPrincipal();

        return scanService.addScan(method, wavelengths, blankImage, sampleImage, sampleName, user.getUserId());
    }

    /**
     * Handles requests to share a recorded measurement
     * Users to share with are identified by email.
     *
     * @param scanId ID of measurement to be shared
     * @param emails A list of emails to share the measurement with
     * @return A response indicating whether sharing was successful or not
     */
    @PostMapping(value = "/analysis/share")
    public ResponseEntity share(@RequestParam long scanId, @RequestParam ArrayList<String> emails){
        
        if(emails==null){
            System.out.println("Info could not be shared. No emails provided.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        scanService.share(scanId, emails);

        System.out.println("Sharing successful");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles requests to delete a scan.
     * Deletion is always successful.
     *
     * @param scanId ID of scan to be deleted
     * @return A response indicating successful deletion
     */
    @PostMapping(value = "/analysis/delete")
    public ResponseEntity deleteScan(@RequestParam long scanId){

        scanService.deleteById(scanId);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    /**
     * Handles requests to obtain absorbance and transmittance values for each wavelength.
     *
     * @param scanId
     * @return A map containing wavelength-to-absorbance and wavelength-to-transmittance pairs
     */
    @GetMapping(value="/analysis/info")
    public Map<String, SignalsDTO> getScanSignals(@RequestParam Long scanId){

        SignalsDTO transmit = signalService.getTransmittanceDTO(scanId);
        SignalsDTO absorbance = signalService.getAbsorbanceDTO(scanId);

        Map<String, SignalsDTO> mapSignal = new HashMap<>();
        mapSignal.put("transmittance", transmit);
        mapSignal.put("absorbance", absorbance);
        return mapSignal;

    }

    /**
     * Handles requests to download a scan.
     * A file is created, sent to the user, and then deleted from the server.
     *
     * @param scanId
     * @return
     * @throws IOException
     */
    @GetMapping(value="/analysis/download")
    public Resource downloadScan(@RequestParam Long scanId) throws IOException {
        String fileName = signalService.makeSignalsCSV(scanId);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(fileName));

        File file = new File(fileName);
        file.delete();
        return resource;
    }
}
