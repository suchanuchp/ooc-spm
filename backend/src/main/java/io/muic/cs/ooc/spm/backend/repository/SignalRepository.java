package io.muic.cs.ooc.spm.backend.repository;


import io.muic.cs.ooc.spm.backend.entity.Signal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface SignalRepository extends CrudRepository<Signal, Long> {
}
