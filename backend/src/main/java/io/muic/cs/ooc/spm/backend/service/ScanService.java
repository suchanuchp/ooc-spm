package io.muic.cs.ooc.spm.backend.service;

import io.muic.cs.ooc.spm.backend.dto.ScanDTO;
import io.muic.cs.ooc.spm.backend.dto.UserDTO;
import io.muic.cs.ooc.spm.backend.entity.*;
import io.muic.cs.ooc.spm.backend.repository.ScanRepository;
import io.muic.cs.ooc.spm.backend.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;


@Service("scanService")
public class ScanService {

    @Autowired
    ScanRepository scanRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    WavelengthService wavelengthService;

    @Autowired
    private ModelMapper modelMapper;


    @Autowired
    SignalService signalService;

    @Autowired
    ImageService imageService;

    @Autowired
    UserService userService;


    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }


    public Long addScan(String method, ArrayList<Integer> inputWavelengths, MultipartFile blankImage,
                        MultipartFile sampleImage, String sampleName, Long ownerId) {

        List<Wavelength> wavelengthList = wavelengthService.createWavelengthArray(inputWavelengths);
        wavelengthService.saveWavelengths(wavelengthList);

        Set<Signal> signals = imageService.calculate(blankImage, sampleImage, wavelengthList);
        signalService.saveSignals(signals);

        Scan scan = new Scan(method, wavelengthList, sampleName, ownerId, signals);
        scanRepository.save(scan);

        userService.setScan(scan, ownerId);



        return scan.getScanId();


    }

    /**
     * Get user's previous scans
     * @param user
     * @return
     */
    public List<ScanDTO> getScans(User user) {

        List<Scan> scans = findScanByUserId(user.getUserId());
        List<ScanDTO> scansDTO = new ArrayList<>();
        for (Scan scan : scans) {

            ScanDTO sc = modelMapper.map(scan, ScanDTO.class);
            mapScanToDTO(scan, sc);

            scansDTO.add(sc);

        }

        return sortScansDTOByTime(scansDTO);

    }


    public void mapScanToDTO(Scan scan, ScanDTO scanDTO) {

        scanDTO.setTime(scan.getTime());

        List<Integer> DTOWavelengths = new ArrayList<>();
        List<Wavelength> wavelengths = scan.getWavelengths();
        for (Wavelength wave : wavelengths) {
            DTOWavelengths.add(wave.getLength());
        }
        scanDTO.setWaves(DTOWavelengths);

        User owner = userService.findById(scan.getUserId());
        UserDTO ownerDTO = modelMapper.map(owner, UserDTO.class);
        Set<User> sharedUsers = scan.getSharedUsers();

        Set<UserDTO> sharedUsersDTO = new HashSet<>();
        for(User user : sharedUsers){
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);
            sharedUsersDTO.add(userDTO);
        }
        scanDTO.setSharedUsersDTO(sharedUsersDTO);
        scanDTO.setOwner(ownerDTO);




    }


    public void share(long scanId, ArrayList<String> emails){

        Set<User> sharedUsers = userService.findByEmails(emails);
        Scan scan = scanRepository.findByScanId(scanId);
        scan.setSharedUsers(sharedUsers);
        scanRepository.save(scan);
        userService.shareScan(sharedUsers, scan);


    }



    public List<ScanDTO> sortScansDTOByTime (List < ScanDTO > scans) {
        SimpleDateFormat f = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        List<ScanDTO> sorted = scans.stream().sorted(
                (a, b) ->
                {
                    try {

                        return f.parse(a.getTime()).compareTo(f.parse(b.getTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
        ).collect(Collectors.toList());
        Collections.reverse(sorted);
        return sorted;
    }


    public List<Scan> findScanByUserId (Long userId){
        return scanRepository.findByUserId(userId);
    }


    public void deleteById(long scanId) {
        scanRepository.deleteById(scanId);
    }


}


