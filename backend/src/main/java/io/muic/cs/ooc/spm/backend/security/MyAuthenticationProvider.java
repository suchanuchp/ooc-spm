package io.muic.cs.ooc.spm.backend.security;

import io.muic.cs.ooc.spm.backend.entity.User;
import io.muic.cs.ooc.spm.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        List<GrantedAuthority> authorities = new ArrayList<>();

        User user = userRepository.findByUsername((String) authentication.getPrincipal());

        if (authentication.getCredentials() == null || user == null){
            throw new BadCredentialsException("Username or password mismatched");
        }
        else if (JBcryptPassGenerator.checkPass((String)authentication.getCredentials(),user.getHashedPassword()) ) {
            // user is authenticated
            if (((String)authentication.getPrincipal()).equals("admin")) {
                authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            }
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            System.out.println("here");
        } else {
            throw new BadCredentialsException("Username or password mismatched");
        }

        return new UsernamePasswordAuthenticationToken(
                user,
                null,
                authorities);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;

    }
}
