package io.muic.cs.ooc.spm.backend.controller;

import io.muic.cs.ooc.spm.backend.dto.ScanDTO;
import io.muic.cs.ooc.spm.backend.dto.UserDTO;
import io.muic.cs.ooc.spm.backend.entity.User;
import io.muic.cs.ooc.spm.backend.service.ScanService;
import io.muic.cs.ooc.spm.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * UserController handles incoming requests to
 * register a new user,
 * retrieve a list of shared scans,
 * and retrieve a list of previous scans of a user.
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ScanService scanService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Sends a request with parameters to register a new user.
     * Successful if the requested username and email do not already exist
     * and all parameters are given.
     *
     * @param username
     * @param password
     * @param email
     * @param firstName
     * @param lastName
     * @return A response indicating whether registration was successful or not
     */
    @PostMapping(value = "/register")
    public ResponseEntity register(@RequestParam("username") String username,
                                   @RequestParam("password") String password,
                                   @RequestParam("email") String email,
                                   @RequestParam("firstName") String firstName,
                                   @RequestParam("lastName") String lastName) {

        User user = userService.register(username, password, firstName, lastName, email);

        if (user == null) {
            System.out.println("New user registration failed.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        System.out.println("Registration successful.");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Sends a request to retrieve a user's shared scans.
     *
     * @param auth
     * @return A list of shared scans
     */
    @GetMapping(value = "/analysis/sharedWithMe")
    public List<ScanDTO> showSharedScanHistory(Authentication auth){
        User user = (User) auth.getPrincipal();
        return userService.getSharedScans(user);
    }

    /**
     * Sends a request to retrieve a user's previous scans.
     *
     * @param auth
     * @return A list of scans
     */
    @GetMapping(value = "/analysis/myData")
    public List<ScanDTO> showScanHistory(Authentication auth){
        User user = (User) auth.getPrincipal();
        return scanService.getScans(user);
    }

    @GetMapping(value = "/userInfo")
    public UserDTO getUserInfo(Authentication auth) {
        User user = (User) auth.getPrincipal();
        return userService.getUserInfo(user);

    }

}
