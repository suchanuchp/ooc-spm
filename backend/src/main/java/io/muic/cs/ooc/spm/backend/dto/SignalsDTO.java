package io.muic.cs.ooc.spm.backend.dto;

import java.util.List;


/**
 * Stores a collection of wavelengths
 * and corresponding transmittance OR absorbance values
 * for plotting.
 */
public class SignalsDTO {

    public SignalsDTO(List<Integer> waveLengths, List<Float> values) {
        this.waveLengths = waveLengths;
        this.signal = values;
    }

    public List<Integer> getWaveLengths() {
        return waveLengths;
    }

    public void setWaveLengths(List<Integer> waveLengths) {
        this.waveLengths = waveLengths;
    }

    public List<Float> getSignal() {
        return signal;
    }

    public void setSignal(List<Float> signal) {
        this.signal = signal;
    }

    private List<Integer> waveLengths;

    private List<Float> signal;

}
