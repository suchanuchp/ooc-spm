package io.muic.cs.ooc.spm.backend.service;


import io.muic.cs.ooc.spm.backend.entity.Wavelength;
import io.muic.cs.ooc.spm.backend.repository.WavelengthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("wavelengthService")
public class WavelengthService {

    @Autowired
    WavelengthRepository wavelengthRepository;

    public void saveWavelengths(List<Wavelength> wavelengths){
        for(Wavelength wl : wavelengths){
            wavelengthRepository.save(wl);
        }
    }

    public List<Wavelength> createWavelengthArray(List<Integer> inputWavelengths){
        List<Wavelength> wavelengthList = new ArrayList<>();
        for(Integer n: inputWavelengths){
            Wavelength wl = new Wavelength(n);
            wavelengthList.add(wl);
        }
        return wavelengthList;
    }

}
