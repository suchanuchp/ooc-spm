package io.muic.cs.ooc.spm.backend.dto;

public enum SignalType {
    ABSORBANCE, TRANSMITTANCE;
}
