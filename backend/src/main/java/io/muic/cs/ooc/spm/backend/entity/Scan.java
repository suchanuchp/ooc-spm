package io.muic.cs.ooc.spm.backend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.data.util.Pair;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;


@Entity
@Table(name="scan")
public class Scan {

    @Id
    @GeneratedValue
    private Long scanId;

    private Long userId; //owner


    private String sampleName;

    @JsonBackReference
    @OneToMany
    private Set<Signal> signals;

    @Enumerated(EnumType.STRING)
    private MethodType methodType;


    @OneToMany(fetch = FetchType.EAGER)
    private List<Wavelength> wavelengths = new ArrayList<>();



    @JsonBackReference
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "join_scan_sharedUsers",
            joinColumns = @JoinColumn(name = "scan_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> sharedUsers = new HashSet<>();


    private LocalDateTime time;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSampleName() {
        return sampleName;
    }

    public void setSampleName(String sampleName) {
        this.sampleName = sampleName;
    }


    public Set<User> getSharedUsers() {
        return sharedUsers;
    }

    public void setSharedUsers(Set<User> sharedUsers) {
        this.sharedUsers.addAll(sharedUsers);
    }
    public void setSharedUsers(User sharedUser) {
        this.sharedUsers.add(sharedUser);
    }

    public List<Wavelength> getWavelengths() {
        return wavelengths;
    }

    public Pair<List<Integer>, List<Float>> getComponents(){
        List<Signal> sortedSignals = new ArrayList<>(signals);
        Collections.sort(sortedSignals, new SignalComparator());

        List<Integer> waveLengths = new ArrayList<>();
        List<Float> vals = new ArrayList<>();

        for (Signal signal : sortedSignals){
            vals.add(signal.getSignal());
            waveLengths.add(signal.getWavelength());
        }
        return Pair.of(waveLengths, vals);
    }



    public void setWavelengths(List<Wavelength> wavelengths){
        if(wavelengths==null) return;
        this.wavelengths.addAll(wavelengths);

    }




    public MethodType getMethodType() {

        return methodType;
    }
    public Long getScanId() {
        return scanId;
    }



    public void setMethodType(String method) {
        if(method.equals("lambda")){
            this.methodType = MethodType.lambda;
        }
        else{
            this.methodType = MethodType.scan;
        }

    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }



    public Set<Signal> getSignals() {
        return signals;
    }

    public void setSignals(Set<Signal> signals) {
        this.signals = signals;
    }

    private class SignalComparator implements Comparator<Signal>{
        @Override
        public int compare(Signal s1, Signal s2) {
            if(s1.getWavelength() < s2.getWavelength()){
                return 1;
            } else {
                return -1;
            }
        }
    }

    public Scan(){}

    public Scan(String method, List<Wavelength> wavelengths, String sampleName, Long ownerId, Set<Signal> signals){
        setMethodType(method);
        setWavelengths(wavelengths);
        setSampleName(sampleName);
        setTime(LocalDateTime.now());
        setUserId(ownerId);
        setSignals(signals);
    }
}
