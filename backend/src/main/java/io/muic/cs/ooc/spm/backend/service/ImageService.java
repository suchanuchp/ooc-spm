package io.muic.cs.ooc.spm.backend.service;

import io.muic.cs.ooc.spm.backend.entity.Signal;
import io.muic.cs.ooc.spm.backend.entity.Wavelength;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

@Service("calculationService")
public class ImageService {

    private static final int WAVELENGTH_RANGE = 300;
    private static final int MIN_WAVELENGTH = 400;
    public static final ArrayList<Integer> DEFAULT_WAVELENGTHS = new ArrayList<Integer>(){{
        for(int i=0; i<WAVELENGTH_RANGE+1; i++){
            add(i+MIN_WAVELENGTH);
        }
    }};

    public Set<Signal> calculate (MultipartFile blankImage, MultipartFile
            sampleImage, List<Wavelength> wavelengths){

        SpectrumImage blank = new SpectrumImage(blankImage);
        SpectrumImage sample = new SpectrumImage(sampleImage);

        float[] blankIntensities = blank.getIntensityArray(wavelengths);
        float[] sampleIntensities = sample.getIntensityArray(wavelengths);
        ArrayList<Float> tmp = new ArrayList<>();
        Set<Signal> signals = new HashSet<Signal>();

        for (int i = 0; i < wavelengths.size(); i++) {
            float value =  sampleIntensities[i] / blankIntensities[i];
            tmp.add(value);
//            signals.add(new Signal(wavelength, value));
        }

        float maxValue = Collections.max(tmp);

        for (int i = 0; i < wavelengths.size(); i++){
            Integer wavelength = wavelengths.get(i).getLength();
            float transmittance = tmp.get(i)/maxValue;
            signals.add(new Signal(wavelength, transmittance));
        }

        return signals;

    }

    private class SpectrumImage {

        private BufferedImage image;

        private SpectrumImage(MultipartFile multipartFile) {

            BufferedImage image = multipartFileToBufferedImage(multipartFile);
            this.image = preprocessing(image);
        }

        private BufferedImage multipartFileToBufferedImage(MultipartFile multipartFile) {
            BufferedImage image = null;
            try {
                File file = File.createTempFile("tmp", ".jpg");
                FileUtils.writeByteArrayToFile(file, multipartFile.getBytes());
                image = ImageIO.read(file);
                file.delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return image;
        }

        private BufferedImage preprocessing(BufferedImage image) {
            return scaleImage(image, WAVELENGTH_RANGE+1, 1);
            // missing calibrate and cropping
        }

        private BufferedImage scaleImage(BufferedImage image, int newX, int newY) {
            BufferedImage scaledImage = new BufferedImage(newX, newY, image.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.drawImage(image, 0, 0, newX, newY, null);
            graphics2D.dispose();
            return scaledImage;
        }

        float[] getIntensityArray(List<Wavelength> wavelengths) {
            float[] values = new float[wavelengths.size()];
            for (int i = 0; i < wavelengths.size(); i++) {
                Color rgb = new Color(image.getRGB(wavelengths.get(i).getLength() - MIN_WAVELENGTH, 0));
                float[] hsv = Color.RGBtoHSB(rgb.getRed(), rgb.getBlue(), rgb.getGreen(), null);
                values[i] = hsv[2];
            }
            return values;
        }

    }
}
