package io.muic.cs.ooc.spm.backend.dto;

import io.muic.cs.ooc.spm.backend.service.SignalService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SignalsDTOFactory {

    @Autowired
    SignalService signalService;

    public SignalsDTO create(List<Integer> wavelengths, List<Float> transmittance, SignalType signalType){
        switch(signalType){
            case ABSORBANCE:
                List<Float> abs = signalService.calculateAbsorbance(transmittance);
                return new SignalsDTO(wavelengths, abs);
            case TRANSMITTANCE:
                return new SignalsDTO(wavelengths, transmittance);
            default:
                return null;
        }
    }

}
