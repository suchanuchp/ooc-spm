package io.muic.cs.ooc.spm.backend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    private FailAuthenticate failAuthenticate;
    private SuccessAuthenticate successAuthenticate;
    private MyAuthenticationProvider myAuthenticationProvider;

    @Autowired
    public SecurityConfiguration(RestAuthenticationEntryPoint restAuthenticationEntryPoint,
                                 FailAuthenticate failAuthenticate, SuccessAuthenticate successAuthenticate,
                                 MyAuthenticationProvider myAuthenticationProvider) {
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
        this.failAuthenticate = failAuthenticate;
        this.successAuthenticate = successAuthenticate;
        this.myAuthenticationProvider = myAuthenticationProvider;
    }

    @Bean
    protected AuthenticationProvider authenticationProvider() {
        return myAuthenticationProvider;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors()
                .and()
                .httpBasic()

                .and()
                .authorizeRequests()
                .antMatchers("/login", "/logout", "/register").permitAll()
                .antMatchers("/**").authenticated()
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginProcessingUrl("/login").permitAll()
                .successHandler(successAuthenticate).failureHandler(failAuthenticate)
                .usernameParameter("username").passwordParameter("password");


        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // This is missing and is important
                .permitAll()
                .invalidateHttpSession(true)
                .deleteCookies("SESSIONID")
                .and()
                .csrf()
                .disable();

        http.csrf().disable();


        http
                .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint);


    }
}
