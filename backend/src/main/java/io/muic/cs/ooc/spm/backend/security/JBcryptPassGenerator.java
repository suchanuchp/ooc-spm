package io.muic.cs.ooc.spm.backend.security;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class JBcryptPassGenerator {

    public static String hashPassword(String plainPassword){
        return BCrypt.hashpw(plainPassword, BCrypt.gensalt());
    }

    public static boolean checkPass(String plainPassword, String hashedPassword) {
        return BCrypt.checkpw(plainPassword, hashedPassword);

    }

}
