package io.muic.cs.ooc.spm.backend.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


//@Data
//@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long userId;

    @Column(nullable = false, unique=true, columnDefinition = "varchar(255)")
    private String username;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String hashedPassword;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "join_user_sharedScans",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "scan_id")
    )
    private Set<Scan> sharedScans = new HashSet<>();

    @JsonBackReference
    @OneToMany
    private Set<Scan> scans = new HashSet<>();

    public User(String username, String firstName, String lastName, String email, String hashedPassword) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.hashedPassword = hashedPassword;

    }
    public User(){}


    public Set<Scan> getSharedScans() {
        return sharedScans;
    }

    public void addSharedScans(Set<Scan> sharedScans) {
        this.sharedScans.addAll(sharedScans);
    }

    public void addSharedScan(Scan sharedScan) {
        this.sharedScans.add(sharedScan);
    }

    public Set<Scan> getScans() {
        return scans;
    }

    public void addScans(Set<Scan> scans) {
        this.scans.addAll(scans);
    }

    public void addScan(Scan scan) {
        this.scans.add(scan);
    }

    public Long getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }
}
