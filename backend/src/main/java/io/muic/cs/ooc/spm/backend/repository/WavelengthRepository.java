package io.muic.cs.ooc.spm.backend.repository;

import io.muic.cs.ooc.spm.backend.entity.Wavelength;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WavelengthRepository extends CrudRepository<Wavelength, Long> {
}
