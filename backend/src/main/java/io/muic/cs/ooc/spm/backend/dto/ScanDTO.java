package io.muic.cs.ooc.spm.backend.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;

public class ScanDTO {

    private static final String DATE_FORMATTER= "d MMM yyyy HH:mm:ss";

    private String sampleName;

    private String methodType;

    private UserDTO owner;

    private Set<UserDTO> sharedUsersDTO;

    private Long scanId;

    private String time;


    public UserDTO getOwner() {
        return owner;
    }

    public void setOwner(UserDTO owner) {
        this.owner = owner;
    }

    public Set<UserDTO> getSharedUsersDTO() {
        return sharedUsersDTO;
    }

    public void setSharedUsersDTO(Set<UserDTO> sharedUsersDTO) {
        this.sharedUsersDTO = sharedUsersDTO;
    }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

    private List<Integer> waves;

    public String getSampleName() {
        return sampleName;
    }

    public void setSampleName(String sampleName) {
        this.sampleName = sampleName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDateTime = time.format(formatter);
        this.time = formatDateTime;

    }

    public List<Integer> getWaves() {
        return waves;
    }

    public void setWaves(List<Integer> waves) {
        this.waves = waves;
    }

    public Long getScanId() {
        return scanId;
    }

    public void setScanId(Long scanId) {
        this.scanId = scanId;
    }

}
