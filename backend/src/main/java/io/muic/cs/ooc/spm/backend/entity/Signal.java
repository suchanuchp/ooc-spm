package io.muic.cs.ooc.spm.backend.entity;


import javax.persistence.*;

@Entity
@Table(name="signals")
public class Signal {

    public Signal(){

    }

    @Id
    @GeneratedValue
    private Long signalId;

    private int wavelength;

    private float value;

    public Signal(int wavelength, float value) {
        this.wavelength = wavelength;
        this.value = value;
    }

    public Long getSignalId() {
        return signalId;
    }

    public int getWavelength() {
        return wavelength;
    }

    public float getSignal() {
        return value;
    }

}
