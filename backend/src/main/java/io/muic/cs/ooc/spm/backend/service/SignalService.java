package io.muic.cs.ooc.spm.backend.service;

import com.opencsv.CSVWriter;
import io.muic.cs.ooc.spm.backend.dto.SignalsDTO;
import io.muic.cs.ooc.spm.backend.dto.SignalsDTOFactory;
import io.muic.cs.ooc.spm.backend.dto.SignalType;
import io.muic.cs.ooc.spm.backend.entity.Scan;
import io.muic.cs.ooc.spm.backend.entity.Signal;
import io.muic.cs.ooc.spm.backend.repository.ScanRepository;
import io.muic.cs.ooc.spm.backend.repository.SignalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("signalService")
public class SignalService {

    @Autowired
    SignalRepository signalRepository;

    @Autowired
    ScanRepository scanRepository;

    @Autowired
    SignalsDTOFactory signalsDTOFactory;

    public String makeSignalsCSV(Long scanId) throws IOException {
        Scan scan = scanRepository.findByScanId(scanId);
        Pair<List<Integer>, List<Float>> comp = scan.getComponents();
        List<Integer> waveLengths = comp.getFirst();
        List<Float> transmittance = comp.getSecond();
        List<Float> absorbance = calculateAbsorbance(transmittance);

        CSVWriter csvWriter = new CSVWriter(new FileWriter("scan_" + scanId + ".csv"),',','\0');
        String[] header = { "Wavelength (nm)", "Transmittance", "Absorbance"};
        csvWriter.writeNext(header);
        for (int i=0; i<waveLengths.size(); i++) {
            csvWriter.writeNext(new String[]{waveLengths.get(i).toString(), transmittance.get(i).toString(), absorbance.get(i).toString()});
        }
        csvWriter.close();
        return "scan_" + scanId + ".csv"; //returns name of the CSV file in project root path
    }

    public SignalsDTO getTransmittanceDTO(Long scanId) {
        Scan scan = scanRepository.findByScanId(scanId);
        Pair<List<Integer>, List<Float>> comp = scan.getComponents();
        List<Integer> waveLengths = comp.getFirst();
        List<Float> vals = comp.getSecond();
        return signalsDTOFactory.create(waveLengths, vals, SignalType.TRANSMITTANCE);
    }

    public SignalsDTO getAbsorbanceDTO(Long scanId){
        Scan scan = scanRepository.findByScanId(scanId);
        Pair<List<Integer>, List<Float>> comp = scan.getComponents();
        List<Integer> waveLengths = comp.getFirst();
        List<Float> vals = comp.getSecond();
        return signalsDTOFactory.create(waveLengths, vals, SignalType.ABSORBANCE);
    }

    public List<Float> calculateAbsorbance(List<Float> transmittance){
        List<Float> result = transmittance
                .stream()
                .map(value -> (float) -Math.log(value))
                .collect(Collectors.toList());
        return result;
    }

    public void saveSignals(Set<Signal> signals){
        for(Signal signal: signals){
            signalRepository.save(signal);
        }
    }
}
