package io.muic.cs.ooc.spm.backend.service;


import io.muic.cs.ooc.spm.backend.dto.ScanDTO;
import io.muic.cs.ooc.spm.backend.dto.UserDTO;
import io.muic.cs.ooc.spm.backend.entity.Scan;
import io.muic.cs.ooc.spm.backend.entity.User;
import io.muic.cs.ooc.spm.backend.repository.UserRepository;
import io.muic.cs.ooc.spm.backend.security.JBcryptPassGenerator;
import org.apache.maven.artifact.repository.Authentication;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userService")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ScanService scanService;

    @Autowired
    private ModelMapper modelMapper;


    public User register(String username, String password, String firstName,
                         String lastName, String email) {

        User user = null;
        if(userRepository.findByUsername(username)==null &&
        userRepository.findByEmail(email)==null&&
        (!username.isEmpty() && !password.isEmpty() && !firstName.isEmpty()
          && !lastName.isEmpty() && !email.isEmpty()) ) {

            String hashedPass = JBcryptPassGenerator.hashPassword(password);

            user = new User(username, firstName, lastName, email, hashedPass);
            userRepository.save(user);

        }
        return user;
    }

    public User findById(Long id){
        return userRepository.findByUserId(id);
    }

    public Set<User> findByEmails(ArrayList<String> emails){

        Set<User> users = new HashSet<>();
        for (String email : emails){
            User user = userRepository.findByEmail(email);
            if(user!=null){
                users.add(user);
            }
        }
        return users;
    }

    /**
     * Get scans that are shared to this user
     * @param user current user
     * @return
     */
    public List<ScanDTO> getSharedScans(User user) {
        User usr = userRepository.findByUserId(user.getUserId());
        Set<Scan> sharedScans = usr.getSharedScans();
        List<ScanDTO> scansDTO = new ArrayList<>();
        for(Scan scan : sharedScans){
            ScanDTO sc = modelMapper.map(scan, ScanDTO.class);
            scanService.mapScanToDTO(scan, sc);
            scansDTO.add(sc);
        }

        return scanService.sortScansDTOByTime(scansDTO);
    }

    /**
     * Share scans to other users
     * @param users Users to share with
     * @param scan The scan to be shared
     */
    public void shareScan(Set<User> users, Scan scan){
        for(User user : users){
            user.addSharedScan(scan);
            userRepository.save(user);
        }
    }

    public void setScan(Scan scan, Long userId){
        User user = findById(userId);
        user.addScan(scan);
        userRepository.save(user);
    }

    public UserDTO getUserInfo(User user){

        return modelMapper.map(user, UserDTO.class);

    }
}
