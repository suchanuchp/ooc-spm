package io.muic.cs.ooc.spm.backend.repository;
import io.muic.cs.ooc.spm.backend.entity.Scan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ScanRepository extends CrudRepository<Scan, Long> {

    List<Scan> findByUserId(Long userId);

    Scan findByScanId(Long scanId);

}
